package notificacion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import observer.Observador;
import state.Automata;

public class MecanismoLoader {
	
	public static List <Observador> cargarRutas(String nombre) throws ClassNotFoundException, InstantiationException, IllegalAccessException  {
		List<Observador> observadores = new ArrayList<Observador>();
		Automata automata = new Automata();
		
		File archivo = new File(nombre); //path
		if (archivo.isDirectory() || archivo.exists()) {
			File[] list = archivo.listFiles(); 
			for (File file : list) {
				if(esArchivoClass(file)) {
					Class<?> c = cargarRuta(nombre,file);
				      System.out.println(c.getName());
				      	Observador o = (Observador) c.newInstance();
				      	observadores.add(o);
				      	automata.agregarObservador(o);    	
				}
			}
		}
		return observadores;
	 }

	public static Class<?> cargarRuta(String nombrePath, File nombreClass) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		String ruta = nombrePath + "/" + nombreClass.getName();
		String [] path = ruta.split("/");
		String classname = path[3] + "."+ path[4];
		String clase = sacarExtensionArchivo(classname);
		Class<?> c = Class.forName(clase);
		return c;
	}
	

	public static String sacarExtensionArchivo(String archivo) {
		int lastIndexOf = archivo.lastIndexOf(".");
		if (lastIndexOf == -1) {
		      return ""; 
		 }
		return archivo.substring(0, lastIndexOf);
	}

	
	public static boolean esArchivoClass(File nombreArchivo) {
		String extensionClass = ".class";
		if(extensionClass.equals(extensionArchivo(nombreArchivo))) {
			return true;
		}
		return false;
	}
	
	private static String extensionArchivo(File nombreArchivo) {
		String archivo = nombreArchivo.getName();
		int lastIndexOf = archivo.lastIndexOf(".");
		if (lastIndexOf == -1) {
		      return ""; 
		 }
		return archivo.substring(lastIndexOf);
	}
}