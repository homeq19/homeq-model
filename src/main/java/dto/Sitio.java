package dto;

public class Sitio {

	private double latitud;
	private double longitud;
	
	public Sitio(double latitud, double longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public double getLatitud() {
		return latitud;
	}

	public double getLongitud() {
		return longitud;
	}
}