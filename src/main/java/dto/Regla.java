package dto;

public class Regla {

	private int id;
	private Sitio sitio;
	private double distancia;
	
	public Regla(int id, Sitio sitio, double distancia) {
		this.id = id;
		this.sitio = sitio;
		this.distancia = distancia;
	}
	
	public int getId() {
		return id;
	}

	public Sitio getSitio() {
		return sitio;
	}
	
	public double getDistancia() {
		return distancia;
	}
}