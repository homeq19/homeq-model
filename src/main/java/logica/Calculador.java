package logica;

import dto.Sitio;

public class Calculador {

	public static double calcularDistancia(Sitio s1, Sitio s2) {
		double radioTierra = 6371; //en kilometros  
		double dLat = Math.toRadians(s2.getLatitud() - s1.getLatitud());  
		double dLng = Math.toRadians(s2.getLongitud()- s1.getLongitud());  
		double sindLat = Math.sin(dLat / 2);  
		double sindLng = Math.sin(dLng / 2);  
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
		                * Math.cos(Math.toRadians(s1.getLatitud())) * Math.cos(Math.toRadians(s2.getLatitud()));  
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
		double distancia = radioTierra * va2;  
		  
		return distancia;
	} 
}
