package state;

import java.util.ArrayList;
import java.util.List;

import dto.Paciente;
import notificacion.MecanismoLoader;
import observer.Evento;
import observer.Observable;
import observer.Observador;

//Clase contexto: la que va a tener el estado actual
public class Automata implements Observable{

	private Estado estado;
	private List<Observador> observadores;
	private EnCasa enCasa;
	private Fugado fugado;
	
	public Automata() {
		this.enCasa = new EnCasa();
		this.fugado = new Fugado();
		this.estado = enCasa;
		this.observadores = new ArrayList<Observador>();
	}

	public void agregarObservador(Observador o) {
		observadores.add(o);
	}
	
	public void eliminarObservador(Observador o) {
		observadores.remove(o);
	}
	
	public List<Observador> getObservadores() {
		return observadores;
	}
	

	@Override
	public void notificarObservador(Evento evento) {
		for(Observador observador : observadores) {
			observador.update(evento);
		}	
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	public void actualizar(Evento evento) {
//		evento.getEstadoAnterior();
//		System.out.println(evento.getEstadoAnterior());
		setEstado(this.estado);
		
//		evento.getEstadoActual();
//		System.out.println(evento.getEstadoActual());
//		evento.transicion(estadoA, estadoB);
		notificarObservador(evento);
	}
	

	@Override
	public String toString() {
		return "Automata [estado=" + estado + ", observadores=" + observadores + "]";
	}	
}