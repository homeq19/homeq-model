package state;

public class Fugado implements Estado{

	private String estado;
	
	public Fugado() {
		this.estado = "Fugado";
	}
	
	@Override
	public void fugarse() {
		System.out.println(estado);
	}
	
	@Override
	public String toString() {
		return estado;
	}
}