package json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import dto.Paciente;
import dto.Regla;
import dto.Sitio;

public class ArchivosJSON {
	
	public static ArrayList<Paciente> leerJSONPacientes() {
		ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Reader reader = leerArchivo("pacientes.json");
	
		Type type = new TypeToken<List<Paciente>>() {}.getType();
		pacientes = gson.fromJson(reader, type);

		return pacientes;
	}
	
	public static ArrayList<Sitio> leerJSONSitios(String archivo) {
		ArrayList<Sitio> sitios = new ArrayList<Sitio>();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Reader reader = leerArchivo(archivo);
	
		Type type = new TypeToken<List<Sitio>>() {}.getType();
		sitios = gson.fromJson(reader, type);

		return sitios;
	}
		
	public static Set<Regla> leerJSONReglas() {
		Set<Regla> reglas = new HashSet<Regla>();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Reader reader = leerArchivo("reglas.json");
	
		Type type = new TypeToken<Set<Regla>>() {}.getType();
		reglas = gson.fromJson(reader, type);

		return reglas;
	}
		
	public static Reader leerArchivo(String archivo) {
		Reader reader = null;
		try {
			reader = new FileReader(archivo);
		} catch (FileNotFoundException e) { 
			//e.printStackTrace(); 
		}
		return reader;
	}
}