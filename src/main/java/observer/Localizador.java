package observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import dto.Paciente;
import dto.Sitio;
import json.ArchivosJSON;

public class Localizador {
	
	private Map<Paciente,Sitio> ubicacion;
	
	public Localizador() {
		this.ubicacion = new HashMap<Paciente,Sitio>();
	}

	public void actualizarUbicacion(Paciente p1) {
		Sitio ubicacionActual = obtenerCoordenada(); //le asigna una nueva coordenada - prueba
//		System.out.println("\nPaciente:" + p1.getNombre() +" Latitud actual: " +ubicacionActual.getLatitud()+ " / Longitud actual: "+ubicacionActual.getLongitud());
		ubicacion.put(p1, ubicacionActual);
	}

	public Sitio obtenerCoordenada() {
		ArrayList<Sitio> sitios = ArchivosJSON.leerJSONSitios("sitiosRandom.json");
		Random r = new Random();
		int aleatorio = r.nextInt(sitios.size());
		Sitio sitio = sitios.get(aleatorio);
		return sitio;
	}
	
	public Map<Paciente, Sitio> getUbicacion() {
		return ubicacion;
	}
}