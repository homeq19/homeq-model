package observer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import dto.Paciente;
import dto.Regla;
import dto.Sitio;
import json.ArchivosJSON;
import logica.Calculador;
import state.Automata;
import state.Estado;
import state.Fugado;

public class Seguimiento implements Observador{

	private Map<Paciente, Sitio> posicionActual; 
	private Set<Regla> reglas;
	private Map<Paciente, Estado> estados;
	private Localizador localizador;
	private Confinamiento confinamiento;
	private Automata automata;
	
	public Seguimiento(Confinamiento confinamiento, Localizador localizador) {
		this.localizador = localizador;
		this.estados = new HashMap<Paciente,Estado>();
		this.automata = new Automata();
		this.posicionActual = confinamiento.getPosicionDeConfinamiento();
		this.reglas = ArchivosJSON.leerJSONReglas();
	}
	
	@Override
	public void update(Evento evento) {
		localizador.actualizarUbicacion(evento.getPaciente());
		posicionActual = localizador.getUbicacion();
		recorrerUbicaciones(); //recorre y se fija ubicacion con las reglas
	}
	
	public void recorrerUbicaciones() {
//		this.estados = new HashMap<Paciente,Estado>();
		for(Map.Entry<Paciente, Sitio> pos : posicionActual.entrySet()) {
			for (Regla regla: reglas) {
//				System.out.println(Calculador.calcularDistancia(pos.getValue(), regla.getSitio()));
				if(Calculador.calcularDistancia(pos.getValue(), regla.getSitio())> regla.getDistancia()) {
					System.out.println("Cambia el estado a fugado");
//					automata.fugarse();
//					estados.put(pos.getKey(), automata.fugarse());
					//					estados.replace(pos.getKey(), new Fugado());
				}else {
					System.out.println("No se incumplio ninguna regla");
				}
			}
		}
	}	
}
