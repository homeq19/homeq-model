package observer;

public interface Observable {

	public void notificarObservador(Evento evento); //Notificar a todos mis observadores
}
