package observer;

import dto.Paciente;
import state.Estado;

public class Evento {
	
	private Paciente paciente; 
	private Estado estadoAnterior;
	private Estado estadoActual;
	
	public Evento(Paciente paciente, Estado anterior, Estado actual) {
		this.paciente = paciente;
		this.estadoAnterior = anterior;
		this.estadoActual = actual;
	}
	
	public Paciente getPaciente() {
		return paciente;
	}

	public Estado getEstadoAnterior() {
		return estadoAnterior;
	}

	public Estado getEstadoActual() {
		return estadoActual;
	}
}