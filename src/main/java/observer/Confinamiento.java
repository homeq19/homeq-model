package observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dto.Paciente;
import dto.Sitio;
import json.ArchivosJSON;

public class Confinamiento implements Observable{

	private Map<Paciente, Sitio> posicionDeConfinamiento;
	private ArrayList<Observador> observadores;
	
	public Confinamiento(ArrayList<Paciente> pacientes) {
		this.posicionDeConfinamiento = new HashMap<Paciente, Sitio>();
		this.observadores = new ArrayList<Observador>();

		Sitio sitio = null;
		for(Paciente paciente: pacientes) {
			this.posicionDeConfinamiento.put(paciente, sitio); //tenemos que actualizar la coordenada y ponerla aca
			actualizarCoordenadasPaciente(); 
		}		
	}

	public void agregarObservador(Observador o) {
		observadores.add(o);
	}
	
	public void eliminarObservador(Observador o) {
		observadores.remove(o);
	}
	
	@Override
	public void notificarObservador(Evento evento) {
		for (Observador observador : observadores) {
			observador.update(evento);
		}
	}
	
	public Map<Paciente, Sitio> getPosicionDeConfinamiento() {
		return posicionDeConfinamiento;
	}

	public void actualizarCoordenadasPaciente() {
		 for(Map.Entry<Paciente, Sitio> posicionPaciente : posicionDeConfinamiento.entrySet()) {
			 Sitio sitio = obtenerCoordenada();
			 System.out.println("Paciente: "+ posicionPaciente.getKey().getNombre() +" Latitud anterior:"+ sitio.getLatitud() + " / Longitud anterior:"+ sitio.getLongitud());
			 posicionPaciente.setValue(sitio);
		 }
	}
	
	public Sitio obtenerCoordenada() {
		ArrayList<Sitio> sitios = ArchivosJSON.leerJSONSitios("sitios.json");
		Random r = new Random();
		int aleatorio = r.nextInt(sitios.size());
		Sitio sitio = sitios.get(aleatorio);
		return sitio;
	}

	public ArrayList<Observador> getObservadores() {
		return observadores;
	}
}