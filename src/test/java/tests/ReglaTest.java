package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import dto.Regla;
import dto.Sitio;

public class ReglaTest {
	
	Sitio s1 = new Sitio(-34.521700, -58.700949);
	Regla r1 = new Regla(1, s1, 0.500);

	@Test
	public void getIdTest() {
		assertEquals(1, r1.getId());
	}
	
	@Test
	public void getDistanciaTest() {
		assertEquals(0.500, r1.getDistancia(), 0);
	}
	
	@Test
	public void getSitio() {
		assertEquals(-34.521700, r1.getSitio().getLatitud(), 0);
		assertEquals(-58.700949, r1.getSitio().getLongitud(), 0);
	}
}