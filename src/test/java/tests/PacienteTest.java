package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import dto.Paciente;

public class PacienteTest {

	Paciente p1 = new Paciente(1,"Juan", "Perez");
	
	
	@Test
	public void verificarIdPacienteTest() {
		assertEquals(1, p1.getId());
		
	}
	
	@Test
	public void verificarNombrePacienteTest() {
		assertEquals("Juan", p1.getNombre());
	}
	
	@Test
	public void verificarApellidoPacienteTest() {
		assertEquals("Perez", p1.getApellido());
	}
}
