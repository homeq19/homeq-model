package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import dto.Paciente;
import observer.Confinamiento;
import observer.Observador;


public class ConfinamientoTest {
	
	private ArrayList<Paciente> pacientes;
	private Observador o;
	
	@Before
	public void inicioPacientes() {
		pacientes = new ArrayList<Paciente>();
		Paciente p1 = new Paciente(1,"Juan", "Perez");
		Paciente p2 = new Paciente(2,"Laura", "Fernandez");
		pacientes.add(p1);
		pacientes.add(p2);
	}
	

	@Test
	public void agregarObservadorTest() {
		Confinamiento c1 = new Confinamiento(pacientes);
		
		c1.agregarObservador(o);
		assertEquals(1, c1.getObservadores().size()); 
	}
	
	@Test
	public void eliminarObservadorTest() {
		Confinamiento c1 = new Confinamiento(pacientes);
		c1.agregarObservador(o);
		c1.eliminarObservador(o);
		assertEquals(0, c1.getObservadores().size()); 
	}
	
	@Test
	public void obtenerCoordenadaTest() {
		Confinamiento c1 = new Confinamiento(pacientes);
		assertTrue(null != c1.obtenerCoordenada());
	}
	
	@Test
	public void getConfinamientoTest() {
		Confinamiento c1 = new Confinamiento(pacientes);
		assertTrue(2 == c1.getPosicionDeConfinamiento().size());
	}
}