package tests;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.junit.Test;
import json.ArchivosJSON;

public class JSONTest {

	@Test
	public void claseJSONTest() {
		ArchivosJSON json = new ArchivosJSON();
			assertTrue(json!= null);
	}
	
	@Test
	public void leerArchivoExistenteTest() {
		assertTrue(ArchivosJSON.leerArchivo("pacientes.json") != null);
	}
	
	@Test
	public void leerArchivoExistenteSitiosTest() {
		assertTrue(ArchivosJSON.leerArchivo("sitios.json") != null);
	}
	
	@Test
	public void leerArchivoInexistenteTest() {
		assertTrue(ArchivosJSON.leerArchivo("archivoInexistente.json") == null);
	}
	
	
	@Test
	public void archivoExistentePacientesTest() {
		assertTrue(2 == ArchivosJSON.leerJSONPacientes().size());
	}
	
	@Test
	public void archivoExistenteSitiosTest() {
		assertTrue(2 == ArchivosJSON.leerJSONSitios("sitios.json").size());
	}
	
	@Test
	public void archivoExistenteSitiosRandomTest() {
		assertTrue(8 == ArchivosJSON.leerJSONSitios("sitiosRandom.json").size());
	}
	
	@Test
	public void archivoExistenteReglasTest() {
		assertTrue(2 == ArchivosJSON.leerJSONReglas().size());
	}
	
}
