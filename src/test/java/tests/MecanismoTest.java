package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import org.junit.Test;
import notificacion.MecanismoLoader;

public class MecanismoTest {

	@Test
	public void claseTest(){
		MecanismoLoader mecanismoLoader = new MecanismoLoader();
		assertTrue(null != mecanismoLoader);
	}
	
	@Test 
	public void sacarExtensionArchivoTest() {
		String archivo = "archivoTest.class";
		assertEquals("archivoTest", MecanismoLoader.sacarExtensionArchivo(archivo));	
	}

	@Test
	public void sacarExtensionArchivoSinPuntoTest() {
		String archivo = "archivo";
		assertEquals("", MecanismoLoader.sacarExtensionArchivo(archivo));
	}
	
	@Test
	public void noEsArchivoClassSinPuntoTest() {
		File archivo = new File("archivo");
		assertFalse(MecanismoLoader.esArchivoClass(archivo));
	}
}
