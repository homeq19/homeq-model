package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import state.EnCasa;
import state.Fugado;

public class EstadoTest {

	
	@Test 
	public void fugadotoStringTest() {
		Fugado fugado = new Fugado();
		assertEquals("Fugado", fugado.toString());
	}
	
	@Test 
	public void enCasatoStringTest() {
		EnCasa enCasa = new EnCasa();
		assertEquals("En Casa", enCasa.toString());
	}
}