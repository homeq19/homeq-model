package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dto.Sitio;
import json.ArchivosJSON;
import logica.Calculador;

public class CalculadorTest {

	@Test
	public void claseCalculadorTest() {
		Calculador calculador = new Calculador();
			assertTrue(null != calculador);
	}
	
	
	@Test
	public void calcularDistanciasTest() {
		Sitio s1 = new Sitio(2.8, 6);
		Sitio s2 = new Sitio(2, 6);
		assertEquals(Calculador.calcularDistancia(s1, s2),88.95594131564697,0);	
	}
}
