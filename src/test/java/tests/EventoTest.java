package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import dto.Paciente;
import observer.Evento;
import state.EnCasa;
import state.Estado;
import state.Fugado;


public class EventoTest {
	Paciente p1 = new Paciente(1,"Juan", "Perez");
	Estado 	estadoAnterior = new EnCasa();
	Estado estadoActual = new Fugado();
	
	Evento e1 = new Evento(p1, estadoAnterior, estadoActual);

	@Test
	public  void cambioEventosTest() {
		assertNotEquals("No son iguales", e1.getEstadoAnterior(), e1.getEstadoActual());	
	}
	
	@Test
	public void verificarPacienteTest() {
		assertEquals("Ambos son iguales","Juan", e1.getPaciente().getNombre());
		assertEquals("Ambos son iguales","Perez", e1.getPaciente().getApellido());	
	}
	
	

}
