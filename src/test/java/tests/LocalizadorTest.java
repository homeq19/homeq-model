package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import dto.Paciente;
import observer.Confinamiento;
import observer.Localizador;

public class LocalizadorTest {
	
	private ArrayList<Paciente> pacientes; 

	@Before	
	public void inicioPacientes() {
		pacientes = new ArrayList<Paciente>();
		Paciente p1 = new Paciente(1,"Pedro","Lopez");
		pacientes.add(p1);
		Confinamiento confinamiento = new Confinamiento(pacientes);
		confinamiento.actualizarCoordenadasPaciente();	
	}
	
	
//	
	@Test
	public void obtenerCoordenadaTest() {
		Localizador localizador = new Localizador();
		assertTrue(null != localizador.obtenerCoordenada());
	}
	
	@Test
	public void localizadorTest() {
		Localizador localizador = new Localizador();
		assertEquals(0, localizador.getUbicacion().size());
	}
	
	@Test
	public void actualizarUbicacionTest() {
		Localizador localizador = new Localizador();
		localizador.actualizarUbicacion(pacientes.get(0));
		assertEquals(1, localizador.getUbicacion().size());
	}
	

}
