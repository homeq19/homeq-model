package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import dto.Sitio;

public class SitioTest {

	Sitio s1 = new Sitio(-34.521700, -58.700949);
	Sitio s2 = new Sitio(-54.521700, 28.704949);

	@Test
	public void getLatitudTest() {
		assertEquals(-34.521700, s1.getLatitud(),0);
	}
	
	@Test
	public void getLongitudTest() {
		assertEquals(-58.700949, s1.getLongitud(),0);
	}
	
	@Test
	public void testSitios() {
		assertEquals(-34.521700, s1.getLatitud(),0);
		assertEquals(28.704949, s2.getLongitud(),0);
		assertNotEquals(-34.521700, s2.getLatitud());
	}
}
