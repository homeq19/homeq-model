package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dto.Paciente;
import observer.Evento;
import observer.Observador;
import state.Automata;
import state.EnCasa;
import state.Estado;
import state.Fugado;

public class AutomataTest {
	
	Automata a = new Automata();
	Observador o;
	
	Paciente p1 = new Paciente(1,"Juan", "Perez");
	Estado 	estadoAnterior = new EnCasa();
	Estado estadoActual = new Fugado();
	Estado nuevo = new EnCasa();
	Evento e1 = new Evento(p1, estadoAnterior, estadoActual);

	
	@Test
	public void agregarObservadorTest() {
		a.agregarObservador(o);
		assertEquals(1, a.getObservadores().size()); 
	}
	
	@Test
	public void eliminarObservadorTest() {
		a.agregarObservador(o);
		a.eliminarObservador(o);
		assertEquals(0, a.getObservadores().size()); 
	}
}