package iteracion0;

import static org.junit.Assert.assertTrue;
import java.io.File;
import org.junit.Test;
import notificacion.MecanismoLoader;

public class UserStory1 {
	
	@Test 
	public void criterioAceptacion1() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		String path = "src/test/java/carpetaInexistente";
		assertTrue(0 == MecanismoLoader.cargarRutas(path).size());
	}

	
	@Test
	public void criterioAceptacion2() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		String path = "src/test/java/carpetaVacia";
		assertTrue(0 == MecanismoLoader.cargarRutas(path).size());
	}
	
	@Test
	public void criterioAceptacion3Test() {
		File file = new File("src/main/java/notificacion/ObservadorEvento.class");
		assertTrue(MecanismoLoader.esArchivoClass(file));
	}
	
	@Test
	public void criterioAceptacion4Test() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		String ruta = "src/test/java/carpetaArchivoTxt";
		assertTrue(0 == MecanismoLoader.cargarRutas(ruta).size());
	}
}